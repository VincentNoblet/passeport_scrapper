import logging
import random
import time
import os

import typer
import smtplib
import ssl
from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from email.mime.multipart import MIMEMultipart
from fake_useragent import UserAgent
from decorator import decorator
from config import Config

app = typer.Typer()

logger = logging.getLogger(__name__)


@app.command()
def main():
    logger.warning("Hello")


@decorator
def fake_human_delay(func, *args, **kwargs):
    time.sleep(args.pop(0))
    return func(*args, **kwargs)


def wait_random(*w_time):
    if len(w_time) == 1:
        w_time = w_time[0]
    elif len(w_time) == 2:
        w_time = random.uniform(w_time[0], w_time[1])
    else:
        raise ValueError(f"len(w_time) should be in [1, 2]) but actually is {len(w_time)}")

    w_time_random = w_time + random.uniform(0, 0.15)
    if not Config.DISABLE_WAIT_TIME:
        time.sleep(w_time_random)
        logger.warning(f"Waiting {w_time_random}")
    else:
        time.sleep(random.uniform(0.05, 0.1))



def wait_suggestion():
    wait_random(2)


def wait_response_to_page_rendering_quick():
    wait_random(0.5, 1.5)


def wait_response_to_page_rendering_long():
    wait_random(1.5, 3)


def wait_response_to_page_scrolling():
    wait_random(2.5, 5)


def get_unique_element_by_property_and_text(*args, **kwargs):
    elements = get_elements_by_property_and_text(*args, **kwargs)
    assert len(elements) == 1, f"{len(elements)} elements found instead of 1"
    return elements[0]


def get_random_element_by_property_and_text(*args, **kwargs):
    elements = get_elements_by_property_and_text(*args, **kwargs)
    return random.choice(elements)


def get_elements_by_property_and_text(root_element, class_name, class_id, *, text="", wait_timeout=0):
    n_trial, elements, running_time, t0 = 0, [], 0, time.time()
    if Config.SLOW_CONNECTION:
        wait_timeout += 15
    while not n_trial and not elements and running_time <= wait_timeout:
        elements = root_element.find_elements(
            By.XPATH,
            f'.//*[contains(., "{text}") and @{class_name}="{class_id}"]'
        )
        running_time = time.time() - t0

    assert len(elements), f"<{class_name}='{class_id}'> not Found (text='{text}')"
    return elements


def send_email(subject):
    port = 587  # For starttls
    # SMTP port=587, chiffrement='TLS', server='pro1.mail.ovh.net'
    # POP port=995, chiffrement='SSL', server='pro1.mail.ovh.net'
    # IMAP port=993, chiffrement='SSL', server='pro1.mail.ovh.net'
    smtp_server = "pro1.mail.ovh.net"
    sender_email = "vincent@noblet.xyz"
    receiver_email = "vincent@noblet.xyz"
    password = "Vince-Saint-dolay-0206"

    try:
        context = ssl.create_default_context()
        with smtplib.SMTP(smtp_server, port) as server:
            server.ehlo()  # Can be omitted
            server.starttls(context=context)
            server.ehlo()  # Can be omitted
            server.login(sender_email, password)

            message = MIMEMultipart("alternative")
            message["Subject"] = subject
            message["From"] = sender_email
            message["To"] = receiver_email
            # server.send_message(message)
            # server.sendmail(sender_email, receiver_email, 'Subject: {}\n\n{}'.format(subject, text))
            server.sendmail(message["From"], message["To"], message.as_string())
    except Exception as e:
        # Print any error messages to stdout
        logger.exception(e)


def navigate_laroche_bd_return_true_if_available(driver, actions):
    element_cookies = get_unique_element_by_property_and_text(
        driver, 'class', 'cc-btn cc-allow', text='Tous les cookies'
    )
    if element_cookies:
        element_cookies.click()

    for text in [
        "Demande de passeport",
        "1 personne",
        "Il n'y a que des personnes majeures"
    ]:

        element = get_unique_element_by_property_and_text(
            driver,  'class', 'sc-bwzfXH eewSDr', text=text, wait_timeout=10
        )
        actions.move_to_element(element).perform()
        wait_random(0.05)
        element.click()

    input_code_postal = get_unique_element_by_property_and_text(
        driver, 'placeholder', 'Commune ou code postal *'
    )
    while input_code_postal.get_attribute("value"):
        wait_random(0.05)
        input_code_postal.send_keys(Keys.BACK_SPACE)
    search_value = "56130"
    for input_character in f"{search_value} ":
        input_code_postal.send_keys(input_character)
        wait_random(0.1)
    input_code_postal.click()
    wait_random(0.1)
    input_code_postal.send_keys(Keys.BACKSPACE)
    wait_random(0.1)
    input_code_postal.click()

    wait_random(0.1, 0.5)

    get_random_element_by_property_and_text(
        driver, 'role', 'option', wait_timeout=10
    ).click()

    wait_random(0.3)

    rendez_vous_button = get_unique_element_by_property_and_text(
        driver, 'type', "button", text="Je n'arrive pas à faire ma pré-demande et veux quand même prendre rendez-vous"
    )
    rendez_vous_button.click()

    if "Aucun créneau disponible" in driver.page_source:
        return False, ""

    table_elements = get_elements_by_property_and_text(
        driver, 'class', "synbird-table-element", wait_timeout=5
    )
    for element in table_elements:
        first_or_last_date_displayed = element.text
        # if it's a date and not an hour
        if len(first_or_last_date_displayed.split("\n")) == 3:
            day_in_week, day, month = first_or_last_date_displayed.split("\n")
            if month in ["oct.", 'nov.']:
                return True, f"Créneau dispo le {day_in_week} {day} {month} à la Roche BD"

    return False, f"Ping 'Passeport scrapper' ✅ SUCCESS at la Roche BD "

    """
    time_slots = []
    for table_element in table_elements:
        time_slot = table_element.find_elements(By.XPATH, f'.//*[contains(@class, "synbird-time-slot") and contains(@class, "undefined")]')
        if len(time_slot):
            time_slot = time_slot[0]
            time_slots.append(time_slot.text)"""



@app.callback()
def main(
    headless: bool = typer.Option(False, help="Browser is headless ?"),
    dev_mode: bool = typer.Option(False, help="Dev mode ? It will deactivate waiting time."),
    slow_connection: bool = typer.Option(False, help="Slow connection ?"),
):
    logger.setLevel(logging.DEBUG)

    Config.DISABLE_WAIT_TIME = dev_mode
    Config.SLOW_CONNECTION = slow_connection
    n_failures = 0
    n_success = 0
    while True:  # n_success <= 0 and n_failures <= 1:

        options = ChromeOptions()

        """options.headless = True
                # )options.binary_location = "/snap/bin/brave" # --profile-directory=Perso"

                options.add_argument("--disable-dev-shm-usage") # // Résout le problème"""
        #options.add_argument('--remote-debugging-port=9224') #NOT 9222
        options.add_argument('--disable-dev-shm-usage')
        options.headless = headless
        options.add_argument("--no-sandbox")
        #options.add_argument('--profile-directory=profile_default')
        # options.add_argument("--user-data-dir=/home/ubuntu/.config/google-chrome/profile_default")

        # image_preferences = {"profile.managed_default_content_settings.images": 2}
        # options.add_experimental_option("prefs", image_preferences)
        # options.binary_location = "/snap/bin/brave"
        # options.add_argument("--tor")
        ua = UserAgent()
        user_agent = ua.random
        logger.warning(f"User agent: {user_agent}")
        options.add_argument(f'user-agent={user_agent}')

        logger.warning("Initializing driver")
        t0 = time.time()
        driver = webdriver.Chrome(service=Service("/usr/bin/chromedriver"), options=options)
        logger.warning(f"Initializing driver took {time.time() - t0}s")
        actions = ActionChains(driver)
        # driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())

        for cityhall_name, navigate_cityhall_return_true_if_available, cityhall_url in [
            ("La Roche-Bernard",
             navigate_laroche_bd_return_true_if_available,
             "https://www.laroche-bernard.com/demarches-administratives/passeport-carte-nationale-didentite/"
             )
        ]:
            try:
                logger.warning(f"Getting URL {cityhall_url}")
                driver.get(cityhall_url)
                driver.maximize_window()
                wait_random(1.5, 3)
                logger.warning(f"Will navigate through {cityhall_name}")
                available, subject = navigate_cityhall_return_true_if_available(driver, actions)
                if available:
                    os.system(f'notify-send "{subject}" -u critical -t 0')
                else:
                    subject = f"Ping 'Passeport scrapper' ✅ SUCCESS at {cityhall_name} "
                n_success += 1
            except Exception as e:
                logger.exception(e)
                subject = f"Ping 'Passeport scrapper' ❌ FAILED"
                n_failures += 1
            finally:
                send_email(subject)

        # time.sleep(30)
        time.sleep(random.randint(240, 480))

    driver.close()


if __name__ == '__main__':
    typer.run(main)
